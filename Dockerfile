FROM node:10 as builder
WORKDIR /usr/src/app

COPY package*.json ./

COPY src ./src/
COPY public ./public/

RUN npm install

RUN npm run build


FROM nginx:alpine

COPY nginx-conf/nginx.conf etc/nginx
COPY --from=builder /usr/src/app/build /www
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]