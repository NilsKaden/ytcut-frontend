export const API_ENDPOINT = process.env.NODE_ENV === "production" ? 'http://nilsk.dynu.net:1337/yt' : 'http://localhost:1337/yt';
export const LAYOUT_MAXWIDTH = 1200;
export const PLAYER_PROGRESS_INTERVAL = 10;
