import React, { useState } from 'react';
import "./FancyHeadline.scss";

const FancyHeadline = ({ renderSmall, toggleDark, isDark, thisIsJavascript }) => {
  const [counter, setCounter] = useState(0);

  const handleClick = () => {
    toggleDark(!isDark);

    if (counter >= 7) {
      thisIsJavascript();
      setCounter(0);
    } else {
      setCounter(counter + 1);
    }
  }

  return (
    <div type="button" onClick={handleClick} className={`FancyHeadline${renderSmall ?  " FancyHeadline--small" : ""}`}>
      <h1>
        <span className="FancyHeadline_cutr">
          cutr
        </span>
        <span className="FancyHeadline_yt">
          .YT
        </span>
      </h1>
    </div>
  );
};

export default FancyHeadline;