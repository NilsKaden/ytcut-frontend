import React from 'react';
import "./URLControls.scss";

const URLControls = ({ url, setUrl }) => {
    return (
        <input name="url" placeholder="enter youtube url" value={url} onChange={(e) => setUrl(e.target.value)} className="url" />
    );
};

export default URLControls;