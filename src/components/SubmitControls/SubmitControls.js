import React from 'react';
import './SubmitControls.scss';

const SubmitControls = ({ endpoint, start, end, url }) => {
    return (
        <a
            className="SubmitControls_submit"
            href={`${endpoint}?url=${encodeURI(url)}&start=${start}&end=${end}`}
            download
        >
            Download
        </a>
    );
};

export default SubmitControls;