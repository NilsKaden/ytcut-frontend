import React, { useState, useEffect } from 'react';
import "./TimeControls.scss";

const TimeControls = ({ playerRef, setStart, setEnd, setPlaying, setLooping, start, end, currentTime }) => {
    const [loopToggle, setLoopToggle] = useState(false);

    const handleStartButton = () => {
        if (playerRef && playerRef.current) {
            const time = playerRef.current.getCurrentTime()
            setStart(Number(Number(time).toFixed(2)))
        }
    }

    const handleEndButton = () => {
        if (playerRef && playerRef.current) {
            const time = playerRef.current.getCurrentTime()
            setEnd(Number(Number(time).toFixed(2)))
        }
    }

    useEffect(() => {
        if (loopToggle) {
            setLooping(true);
            setPlaying(true);
        } else {
            setLooping(false);
        }
    }, [loopToggle, setLooping, setPlaying]);

    return (
        <div className="TimeControls">
            <div className="timer">
                <input value={currentTime || "current time"} placeholder="current time" disabled  readOnly />
            </div>

            <div className="start">
                <label htmlFor="start" className="start__label">start</label>
                <input type="number" step="0.01" name="start" placeholder="start time in sec." value={start} onChange={e => setStart(Number(e.target.value))} className="start__input"/>
                <button type="button" onClick={handleStartButton} className="start__button">start</button>
            </div>

            <div className="end">
                <label htmlFor="end" className="end__label">end</label>
                <input type="number" step="0.01" name="end" placeholder="end time in sec." value={end} onChange={e => setEnd(Number(e.target.value))} className="end__input" />
                <button type="button" onClick={handleEndButton} className="end__button">end</button>
            </div>
            <div className="loopControls">
                <span>Loop:</span>
                <label className="switch">
                    <input type="checkbox" onClick={() => setLoopToggle(!loopToggle)}></input>
                    <span className="slider"></span>
                </label>
            </div>
      </div>
    );
};

export default TimeControls;