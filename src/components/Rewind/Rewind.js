import React from 'react';
import './Rewind.scss';

const Rewind = ({ isActive }) => {
  return (
    <div className="rewind_wrapper">
      <svg className={isActive ? 'rewind rewind--active' : 'rewind rewind--hidden'} xmlns="http://www.w3.org/2000/svg" width="488" height="488" viewBox="0 0 488.1 488.1"><g className="rewind_svg"><path d="M258.8 227.3L459.1 60.1c3-1.7 6.3-2.6 9.7-2.6 3.3 0 6.7 0.9 9.7 2.6 6 3.5 9.7 9.8 9.7 16.7v334.4c0 6.9-3.7 13.3-9.7 16.7 -6 3.4-13.3 3.4-19.3 0L258.8 260.8c-6-3.4-9.7-9.8-9.7-16.7C249.1 237.2 252.8 230.8 258.8 227.3zM0 244.1c0 6.9 3.7 13.3 9.7 16.7l200.4 167.2c6 3.4 13.3 3.4 19.3 0 6-3.5 9.7-9.8 9.7-16.7V76.9c0-6.9-3.7-13.3-9.7-16.7 -3-1.7-6.3-2.6-9.7-2.6 -3.3 0-6.7 0.9-9.7 2.6L9.7 227.3C3.7 230.8 0 237.2 0 244.1z"/></g></svg>
    </div>
  );
};

export default Rewind;

