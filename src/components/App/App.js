import React, { useState, useRef, useEffect } from 'react';
import ReactPlayer from 'react-player'
import Rewind from '../Rewind/Rewind';
import TimeControls from '../TimeControls/TimeControls';
import URLControls from '../URLControls/URLControls';
import SubmitControls from '../SubmitControls/SubmitControls';
import FancyHeadline from '../FancyHeadline/FancyHeadline';
import './App.scss';
import './Dark.scss';

import { API_ENDPOINT, LAYOUT_MAXWIDTH, PLAYER_PROGRESS_INTERVAL } from '../../config';
console.log(process.env.NODE_ENV);
// TODO: credit this guy: https://www.flaticon.com/free-icon/rewind-symbol_31316#term=rewind&page=1&position=35

const App = () => {
  const [url, setUrl] = useState('');
  const [start, setStart] = useState(0.0);
  const [end, setEnd] = useState(1.0);
  const [currentTime, setCurrentTime] = useState(0.0);
  const [width, setWidth] = useState(0);
  const [playing, setPlaying] = useState(false);
  const [looping, setLooping] = useState(false);
  const [showReverseIcon, setShowReverseIcon] = useState(false);
  const [isDark, toggleDark] = useState(false);

  const ref = useRef(null);
  
  const handleProgress = (e) => {
    const progress = Number(e.playedSeconds.toFixed(2));
    setCurrentTime(progress);
  }

  const playerReady = () => {
    console.log("playerReady");
    // might need finetuning, or a smarter solution
  window.setTimeout(() => {ref.current.seekTo(start, 'seconds'); /*setPlaying(true) */}, 250);
  }

  // this is javascriptuuuuu :)
  const thisIsJavascript = () => {
    console.log('this is javascriptu!');

    setUrl("https://www.youtube.com/watch?v=7QgIIGNcns4");
    setStart(29.5);
    setEnd(35.5);
    setPlaying(true);
  }

  useEffect(() => {
    const body = document.querySelector('body');
    if (isDark) {
      body.className = "dark";
    } else {
      body.className = "";
    }
  }, [isDark])

  // handle rewinds
  useEffect(() => {
    if (looping && currentTime >= end) {
      // show rewind logo for a bit
      setShowReverseIcon(true);

      // rewind to start
      ref.current.seekTo(start, 'seconds');
      setPlaying(true);
      
      // remove verse icon
      window.setTimeout(() => {
        setShowReverseIcon(false);
      }, 700);
    }
  }, [start, end, currentTime, looping])

  // detect resizes, and calculate player size based on window size
  useEffect(() => {
    let resizeTimer;

    const updateSize = () => {
      const innerWidth = window.innerWidth;
      console.log(innerWidth);
      // for mobile: whole width, minus paddings
      if (innerWidth <= 599) {
        setWidth(innerWidth - 20);
      // for mobile -> desktop: Width - Controls - Paddings
      } else if (innerWidth >= 600 && innerWidth < 1200) {
        setWidth(innerWidth - 190);
      // for maxWidth
      } else {
        setWidth(LAYOUT_MAXWIDTH - 190)
      }
    }

    // debounce
    window.addEventListener('resize', () => {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(updateSize, 100);
    });
    updateSize();
  }, []);

  return (
    <div className={`Layout${isDark ? " Layout--dark" : ""}`}>
      <FancyHeadline
        renderSmall={url && url.length > 0}
        toggleDark={toggleDark}
        isDark={isDark}
        thisIsJavascript={thisIsJavascript}
      />
      <Rewind
        isActive={showReverseIcon}
      />
      <div className={`URLControls${(!url || !(url.length > 0)) ? " URLControls--big" : ""}`}>
        <URLControls
          url={url}
          setUrl={setUrl}
        />
      </div> 
      {url && url.length > 0 &&
        <div className="Controls">
          <TimeControls
            playerRef={ref}
            setStart={setStart}
            setEnd={setEnd}
            setPlaying={setPlaying}
            looping={looping}
            setLooping={setLooping}
            start={start}
            end={end}
            currentTime={currentTime}
          />
          <SubmitControls
            endpoint={API_ENDPOINT}
            url={url}
            start={start}
            end={end}
          />
          <a className="Controls_gitlab" target="_blank" rel="noopener noreferrer" href="https://gitlab.com/NilsKaden/ytcut-frontend">view code</a>
        </div>
      }

      <div className="Player">
        {url && url.length > 0 &&
        <ReactPlayer
          width={width}
          height={width / 1.78}
          ref={ref}
          url={url}
          controls={true}
          onProgress={handleProgress}
          progressInterval={PLAYER_PROGRESS_INTERVAL}
          playing={playing}
          onReady={playerReady}
        />}
      </div>
    </div>
  );
}

export default App;
